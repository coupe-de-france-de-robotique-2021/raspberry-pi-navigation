from com import Com

print("Starting")

commandExecuted = False
currentCommand = []

def receiverEvent(command) :
    global commandExecuted
    if command[0] == 0x03 :
        if command == currentCommand :
            commandExecuted = True
    if command[0] == 0x05 :
        print("Pose measured : x = {}, y = {}, th = {}\n".format(*command[1:]), end='')


com = Com('/dev/serial0', 2000000)
com.initReader(receiverEvent)

# READ FILE

f = open("demofile.txt", "r")

for line in f:
    if len(line) != 0 :
        items = line.replace('\n', '').split('\t')

        if items[0] == '0x02' :
            command = [
                int(items[0], 16),
                float(items[1]),
                float(items[2]),
                float(items[3])
                ]

            print("Calibrationg to x = {}, y = {}, th = {}\n".format(*command[1:]), end='')
            com.sendCommand(*command)

        elif items[0] == '0x03' :
            currentCommand = [
                int(items[0], 16),
                int(items[1]),
                float(items[2]),
                float(items[3]),
                float(items[4])
                ]

            print("Moving to x = {}, y = {}, th = {}... ".format(*currentCommand[2:]), end='')

            commandExecuted = False
            com.sendCommand(*currentCommand)

            while not commandExecuted :
                continue

            print("OK")
            #input("Entrer pour passer à l'étape suivante")

        elif items[0] == '0x05' :
            command = [int(items[0], 16)]

            print("Getting measured pose")
                
            com.sendCommand(*command)

        elif items[0] == '0x06' :
            command = [int(items[0], 16)]

            print("Raising Flag")
                
            com.sendCommand(*command)

f.close()

