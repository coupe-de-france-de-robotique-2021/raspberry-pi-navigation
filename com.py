from serial import Serial
from time import sleep
from threading import Thread
from PyQt5.QtCore import *


PORT = '/dev/serial0'
BAUDRATE = 2000000

SIZEOF_UINT32 = 4


'''
============================================
COMMUNICATION PROTOCOL
============================================

Sended Commands
--------------------------------------------
0x01
Is Ready ?
--------------------------------------------
0x02, float, float, float
id, x, y, theta
Calibrate to position (x,y) and angle theta
--------------------------------------------
0x03, uint8, float, float, float
id, directon, x, y, theta
Move to target position (x,y) and target angle theta
direction = 1 : move forward
direction = 0 : move backward
--------------------------------------------
0x04
Stop
--------------------------------------------
0x05
Get current Pose
--------------------------------------------
0x06
Rise flag
--------------------------------------------

============================================

Received Commands
--------------------------------------------
0x01
Is Ready.
--------------------------------------------
0x03, uint8, float, float, float
id, direction,  x, y, theta
Successfully moved to target position (x,y)
and angle theta with direction
--------------------------------------------
0x05, float, float, float
id, x, y, theta
Current Pose
--------------------------------------------

============================================

'''



class Com() :
    def __init__(self, port, baudrate) :
        #Serial
        self.ser = Serial(port, baudrate, timeout=1)

        self.eventFcn = None
        self.receiverRunning = False
        self.threadReader = None


    #DataStream Creator
    def sendCommand(self, *argList):
        '''
        argList structure :
        argList[0] : command type
        argList[1:] : command elements
        '''
        
        paquet = QByteArray()
        out = QDataStream(paquet, QIODevice.WriteOnly)
        
        out.setByteOrder(QDataStream.LittleEndian)

        #Command
        out.writeInt8(argList[0])

        for i in argList[1:] :
            if type(i) == int :
                out.writeInt8(i)
            elif type(i) == float :
                out.writeDouble(i)
            elif type(i) == str :
                out.writeString(i)

        #print(paquet)
        #ser.write(int(0x01).to_bytes(1,'big'))
        self.ser.write(paquet)


    def getCommand(self) :
        msg = self.ser.read()

        command = []

        if len(msg) != 0 :
            #print(msg)

            #GET DATASTREAM
            
            def getStream(ptr) :
                paquet = QByteArray(ptr)
                #paquet.seek(0)
                    
                dataIn = QDataStream(paquet)
                dataIn.setByteOrder(QDataStream.LittleEndian)

                #print(paquet)

                return dataIn

            dataIn = getStream(msg)
            command.append(dataIn.readInt8())
            #command.append(int.from_bytes(msg, "little"))

            #PARSER

            if command[0] == 0x03 :
                
                msg = self.ser.read(1+8*3)
                dataIn = getStream(msg)

                command.append(dataIn.readInt8())
                
                for _ in range(3) :
                    command.append(dataIn.readDouble())


            elif command[0] == 0x05 :
                msg = self.ser.read(8*3)
                dataIn = getStream(msg)

                for _ in range(3) :
                    command.append(dataIn.readDouble())

        return command


    def readerLoop(self) :
        while self.receiverRunning :
            command = self.getCommand()

            if len(command) != 0 :
                self.eventFcn(command)


    def initReader(self, eventFcn) :
        self.receiverRunning = True
        self.eventFcn = eventFcn
        
        self.threadReader = Thread(target=self.readerLoop)
        self.threadReader.start()


    def finishReader(self) :
        self.receiverRunning = False

        self.threadReader.join()

    def __del__(self):
        self.finishReader()

